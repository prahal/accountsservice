# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# 
# Translators:
# Martin Srebotnjak <miles@filmsi.net>, 2013
msgid ""
msgstr ""
"Project-Id-Version: accounts service\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2013-01-17 09:48-0500\n"
"PO-Revision-Date: 2019-02-22 14:19+0000\n"
"Last-Translator: Martin Srebotnjak <miles@filmsi.net>\n"
"Language-Team: Slovenian (http://www.transifex.com/freedesktop/accountsservice/language/sl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: sl\n"
"Plural-Forms: nplurals=4; plural=(n%100==1 ? 0 : n%100==2 ? 1 : n%100==3 || n%100==4 ? 2 : 3);\n"

#: ../data/org.freedesktop.accounts.policy.in.h:1
msgid "Change your own user data"
msgstr "Spremenite lastne uporabniške podatke"

#: ../data/org.freedesktop.accounts.policy.in.h:2
msgid "Authentication is required to change your own user data"
msgstr "Za spremembo lastnih podatkov je zahtevana overitev"

#: ../data/org.freedesktop.accounts.policy.in.h:3
msgid "Manage user accounts"
msgstr "Upravljaj račune uporabnikov"

#: ../data/org.freedesktop.accounts.policy.in.h:4
msgid "Authentication is required to change user data"
msgstr "Za spremembo podatkov uporabnikov je zahtevana overitev"

#: ../data/org.freedesktop.accounts.policy.in.h:5
msgid "Change the login screen configuration"
msgstr "Spremenite nastavitev prijavnega zaslona"

#: ../data/org.freedesktop.accounts.policy.in.h:6
msgid "Authentication is required to change the login screen configuration"
msgstr "Za spremembo prijavnega zaslona je potrebna overitev"

#: ../src/main.c:127
msgid "Output version information and exit"
msgstr "Izpiši podatke o različici in končaj"

#: ../src/main.c:128
msgid "Replace existing instance"
msgstr "Zamenjaj obstoječo pojavitev"

#: ../src/main.c:129
msgid "Enable debugging code"
msgstr "Omogoči razhroščevanje kode"

#: ../src/main.c:149
msgid ""
"Provides D-Bus interfaces for querying and manipulating\n"
"user account information."
msgstr "Zagotavlja vmesnike D-Bus za povpraševanje in upravljanje\ns podatki računov uporabnikov."
